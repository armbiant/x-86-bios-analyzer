from decouple import config
import discord
from discord.ext import commands
import botext



description = '''Discord bot used to process BIOS files.'''
intents = discord.Intents.default()
intents.members = True
intents.message_content = True
bot_version = "1.3a"

bot = commands.Bot(command_prefix='!', description=description, intents=intents)

# bot init message
@bot.event
async def on_ready():
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('----------------------------')

# !ping    
@bot.command()
async def ping(ctx):
    """Checks if the bot is alive"""
    await ctx.reply("Yo, I'm here!", mention_author=False)

# !ver    
@bot.command()
async def ver(ctx):
    """Checks if the bot is alive"""
    await ctx.reply("TRW analyzer bot, version " + bot_version, mention_author=False)

# !b
@bot.command()
async def b(ctx):
    """Analyzes BIOS files and outputs results as text or spreadsheet"""
    if ctx.message.attachments or 'https://' in ctx.message.content:
        await botext.folderMaintenance()
        await botext.getAttachments(ctx)
        await botext.processFiles(ctx, True)    
    else:
        await ctx.send("You forgot to attach/link files, try again.")
# !bns
@bot.command()
async def bns(ctx):
    """Analyzes BIOS files and outputs results only as text"""
    if ctx.message.attachments or 'https://' in ctx.message.content:
        await botext.folderMaintenance()
        await botext.getAttachments(ctx)
        await botext.processFiles(ctx, False)    
    else:
        await ctx.send("You forgot to attach/link files, try again.")

# !split
@bot.command()
async def split(ctx):
    """Splits a BIOS file into ODD/EVEN"""
    if ctx.message.attachments:
        await botext.folderMaintenance()
        await botext.splitFiles(ctx)   
    else:
        await ctx.send("You forgot to attach files, try again.")

# !join
@bot.command()
async def join(ctx):
    """Joins BIOS files into a single one. Upload order: ODD (High) then EVEN (Low)"""
    if ctx.message.attachments:
        await botext.folderMaintenance()
        await botext.joinFiles(ctx)   
    else:
        await ctx.send("You forgot to attach files, try again.")

bot.run(config('token',default=''))
