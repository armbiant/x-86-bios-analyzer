from biostools import __main__
from discord.ext import commands
from decouple import config
from datetime import datetime
import pandas as pd
import discord, os, shutil, urllib, re, openpyxl, unicodedata

input_path = "./roms/1"
output_path = "./roms/0"
status = 0

def csv_process(ctx, lines):
    sheet = open("roms/output.csv", "w")
    for i in range (0, len(lines)):
        item = ""
        for j in range (0, len(lines[i])):
            content = str(lines[i][j])
            item += "\"" + content + "\""
            if j < 6:
                item += ","
            else:
                item += "\n" 
        sheet.write(item)
    sheet.close()
    newname = "roms/TRW_" + ctx.message.author.name + "_" + datetime.now().strftime("%Y%m%d_%H%M%S") + ".xlsx"
    df = pd.read_csv (r'roms/output.csv')
    writer = pd.ExcelWriter(newname) 
    df.to_excel (writer, index = None, header=True, sheet_name='BIOS_analysis')
    # Auto-adjust columns' width
    for column in df:
        column_width = max(df[column].astype(str).map(len).max(), len(column))
        col_idx = df.columns.get_loc(column)
        writer.sheets['BIOS_analysis'].set_column(col_idx, col_idx, column_width)
    writer.save()
    return newname
    

# this function converts the list array into a discord comment reply
def format_response(lines):
    string = "```\n"
    for i in range (0, len(lines[0])):
        header = str(lines[0][i])
        content = str(lines[1][i])
        #very specific empty strings, used just for visual formatting
        if i == 0:
            header += ":     "
        if i == 1 or i == 3:
            header += ":   "
        if i == 2 or i == 4:
            header += ":  "
        if i == 5: 
            header += ": "
            content = content.replace("\n", "\n          ")
        if i == 6: 
            header += ": "
            content = content.replace("\n", "\n      ")
        string += header + content + "\n"
    string += "```"
    return string

def format_multiresponse(lines):
    string = "```\n"
    for i in range (1, len(lines)):
        for j in range (0, len(lines[0])):
            header = "[" + str(i) + "] " + str(lines[0][j])
            content = str(lines[i][j])
            #very specific empty strings, used just for visual formatting
            if j == 0:
                header += ":    "
                string += header + content + "\n"
            if j == 1:
                header = "[" + str(i) + "] Type :  "
                content = str(lines[i][j]) + " " + str(lines[i][j+1])
                string += header + content + "\n"
            if j == 3:
                header += ":  "
                string += header + content + "\n"
            if j == 4:
                header += ": "
                string += header + content + "\n"
                if i < len(lines)-1:
                    string += "-------------------------------" + "\n"
    string += "```"   
    return string

async def folderMaintenance():
    global status
    status = 0
    # creates /1 if it doesn't exist already
    if not os.path.exists(input_path): 
        os.mkdir(input_path)
    # removes old /0 and its contents
    if os.path.exists(output_path):
        shutil.rmtree(output_path)
    status += 1
    
async def getAttachments(ctx):
    global status
    file_count = 0
    # saves all the attachments
    if ctx.message.attachments:
        for attach in ctx.message.attachments:
            file_count += 1 
            await attach.save(f"./roms/1/{attach.filename}")
    # try to download all the files if linked
    if 'https://' in ctx.message.content:
        urls = re.findall('(?P<url>https?://[^\s]+)', ctx.message.content)
        # prevent duplicate downloads with a unique url list
        for url in list(set(urls)):
            filename = url[url.rfind('/')+1: len(url)]
            filename = unicodedata.normalize('NFKD', filename).encode('ascii', 'ignore').decode('ascii')
            filename = re.sub(r'[^\w\s-]', '', filename.lower())
            filename = re.sub(r'[-\s]+', '-', filename).strip('-_')
            try:
                opener = urllib.request.build_opener()
                opener.addheaders = [('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')]
                urllib.request.install_opener(opener)
                urllib.request.urlretrieve(url, input_path + "/" + filename)
                file_count += 1 
            # if the download fails, send a message
            except Exception as e:
                await ctx.send(e, mention_author=False)
    if file_count > 0:
        status += 1


async def processFiles(ctx, sheet):
    global status
    if status == 2:
        botMessage = await ctx.reply("File sent", mention_author=False)
        # message update for each stage of the process
        __main__.main('extract')
        await botMessage.edit(content="Extracting BIOS modules")
        lines = [["File","Vendor","Version","String","Sign-on","Metadata","ROMs"]]
        lines.extend(__main__.main('analyze'))
        # cleans the list from None values
        lines = list(filter(lambda item: item is not None, lines))
        #print(lines)
        await botMessage.edit(content="Analyzing BIOS modules")
        # checking if the analyze step was succesful
        if len(lines) > 1:
            # if a single file was processed, just send a discord text reply
            if len(lines) == 2:
                await botMessage.edit(content=format_response(lines))
            # if there are more files, make a decision
            else:
                if sheet is True:
                    # send a spreadsheet with the results
                    newname = csv_process(ctx, lines)
                    output_sheet = discord.File(newname)
                    await botMessage.edit(content="Multple files processed, here are the results:")
                    await botMessage.add_files(output_sheet)
                    # removes previous sheets, if they exists
                    if os.path.exists('roms/output.csv'):
                        os.remove('roms/output.csv')
                    if os.path.exists(newname):
                        os.remove(newname)
                else:
                    # the bot will reply with a simplified multi row message with the essentials
                    await botMessage.edit(content=format_multiresponse(lines))
        # for some reason the analyze function did not return info, cancels any further actions of the bot
        else:
            await botMessage.edit(content="Oops, something went wrong when analyzing")
    else:
        botMessage = await ctx.reply("Error getting files", mention_author=False)

async def splitFiles(ctx):
    global status
    # saves all the attachments
    if ctx.message.attachments:
        if len(ctx.message.attachments) == 1:
            for attach in ctx.message.attachments:
                await attach.save(f"./roms/1/{attach.filename}")
            botMessage = await ctx.reply("File split", mention_author=False)
            fn1 = "./roms/1/" + attach.filename[:attach.filename.rindex('.')] + "_LO.bin"
            fn2 = "./roms/1/" + attach.filename[:attach.filename.rindex('.')] + "_HI.bin"
            file0 = open("./roms/1/" + attach.filename, "rb")
            file1 = open(fn1, "wb")
            file2 = open(fn2, "wb")
            byte = file0.read(1)
            file1.write(byte)
            byte = file0.read(1)
            file2.write(byte)
            while byte:
                byte = file0.read(1)
                file1.write(byte)
                byte = file0.read(1)
                file2.write(byte)
            file0.close()
            file1.close()
            file2.close()
            f1 = discord.File(fn1)
            f2 = discord.File(fn2)
            botMessage = await ctx.send("`EVEN (LOW)`")
            await botMessage.add_files(f1)
            botMessage2 = await ctx.send("`ODD (HIGH)`")
            await botMessage2.add_files(f2)
            if os.path.exists(input_path):
                shutil.rmtree(input_path)
        else:
            await ctx.reply("Too many files attached", mention_author=False)
    else:
        botMessage = await ctx.reply("Error getting files", mention_author=False)

async def joinFiles(ctx):
    global status
    # saves all the attachments
    if ctx.message.attachments:
        if len(ctx.message.attachments) == 2:
            filename = []
            for attach in ctx.message.attachments:
                filename.append("./roms/1/" + attach.filename)
                await attach.save(f"./roms/1/{attach.filename}")
            botMessage = await ctx.reply("Files joined", mention_author=False)
            file0 = open("./roms/1/OUT.bin", "wb")
            file1 = open(filename[1], "rb")
            file2 = open(filename[0], "rb")
            byte1 = file1.read(1)
            file0.write(byte1)
            byte2 = file2.read(1)
            file0.write(byte2)
            while byte1:
                byte1 = file1.read(1)
                file0.write(byte1)
                byte2 = file2.read(1)
                file0.write(byte2)
            file0.close()
            file1.close()
            file2.close()
            f1 = discord.File('./roms/1/OUT.bin')
            await botMessage.add_files(f1)
            if os.path.exists(input_path):
                shutil.rmtree(input_path)
        else:
            await ctx.reply("Incorrect number of files attached", mention_author=False)
    else:
        botMessage = await ctx.reply("Error getting files", mention_author=False)
